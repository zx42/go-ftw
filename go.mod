module gitlab.com/zx42/go-ftw

go 1.13

// replace gitlab.com/zx42/go-mockfs => ../go-mockfs
// replace gitlab.com/zx42/go-deps-log => ../go-deps-log
// replace gitlab.com/zx42/go-deps => ../go-deps

require (
	github.com/gobwas/glob v0.2.3
	github.com/karrick/godirwalk v1.7.8
	github.com/pkg/errors v0.8.1
	gitlab.com/zx42/go-deps v0.0.3
	gitlab.com/zx42/go-mockfs v0.0.3
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6
)
