// Copyright © 2019 Hendrik Radon <mrcs.sanefiles@mailnull.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package scan_test

import (
	"context"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/pkg/errors"
	. "gitlab.com/zx42/go-mockfs"

	"gitlab.com/zx42/go-ftw"
	"gitlab.com/zx42/go-ftw/scan"
	"gitlab.com/zx42/go-ftw/walk"
)

type (
	aborterT struct {
		abortAt string
		cancel  context.CancelFunc
	}
)

func TestWalkFilesAbort(t *testing.T) {
	tests := []struct {
		name    string
		dir     FE
		abortAt string
		frec    ftw.DirHandler
		expErr  error
	}{
		{name: "abort", dir: D("a", D("b", F("c")), F("d")),
			abortAt: "a/b",
			frec:    scan.FileRec(false, lg).Must("a", "b").MustNot("c", "d"),
			expErr:  context.Canceled,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fs := FS("abort", tt.dir).Create()
			cxt, cancel := context.WithCancel(context.Background())
			ccr := scan.NewContextChecker(cxt)
			aborter := newAborter(cancel, tt.abortAt)
			errH := scan.IgnoreMoreContextErrors(scan.NewErrorPrinter(lg), lg)
			ftwr := walk.NewParDirWalker(lg, errH, ccr, aborter, tt.frec)
			err := ftwr.WalkPar(context.TODO(), fs.Path())
			if errors.Cause(err).Error() != tt.expErr.Error() {
				t.Errorf("WalkFilesAbort(%s) error = '%v', expected '%v'", fs.Path(), err, tt.expErr)
				return
			}

			scan.FinishTest("WalkFilesAbort", t, fs, tt.frec)
		})
	}
}

//
// aborterT struct
//
func newAborter(cancel context.CancelFunc, abortAt string) *aborterT {
	abr := &aborterT{
		abortAt: abortAt,
		cancel:  cancel,
	}
	return abr
}

func (abr *aborterT) OpenDir(_ ftw.AbsFolder) ftw.FileHandler {
	return abr
}

func (abr *aborterT) HandleFile(relPath string, fmode os.FileMode) error {
	if abr.abortAt == "" {
		return nil
	}

	// log.Printf("aborterT.ScanFile(%#v)", fEntry)
	normPath := filepath.ToSlash(relPath)
	if strings.HasSuffix(normPath, abr.abortAt) {
		abr.cancel()
	}

	return nil
}

func (abr *aborterT) Close() error {
	// log.Printf("fileRecorderT.Close(): %#v", fr)
	return nil
}
