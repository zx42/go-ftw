// Copyright © 2019 Hendrik Radon <mrcs.sanefiles@mailnull.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package scan

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"gitlab.com/zx42/go-deps/log"

	"gitlab.com/zx42/go-ftw"
)

type (
	dirRecorderT struct {
		mustFind    []string
		mustNotFind []string
		verbose     bool
		handlerM    map[string]*fileRecorderT
		lg          log.Logger
	}
	fileRecorderT struct {
		// dir   ftw.AbsFolder
		spec  *dirRecorderT
		found map[string]int
		lg    log.Logger
	}
)

//
// fileRecorderT struct
//
func FileRec(verbose bool, lg log.Logger) *dirRecorderT {
	frs := &dirRecorderT{
		verbose: verbose,
		// found:   make(map[string]int),
		handlerM: make(map[string]*fileRecorderT),
		lg:       lg,
	}
	return frs
}

func (frs *dirRecorderT) Must(fnames ...string) *dirRecorderT {
	if frs != nil {
		frs.mustFind = append(frs.mustFind, fnames...)
	}
	return frs
}

func (frs *dirRecorderT) MustNot(fnames ...string) *dirRecorderT {
	if frs != nil {
		frs.mustNotFind = append(frs.mustNotFind, fnames...)
	}
	return frs
}

func (dr *dirRecorderT) OpenDir(folder ftw.AbsFolder) ftw.FileHandler {
	rfr := &fileRecorderT{
		// dir:   folder,
		spec:  dr,
		found: make(map[string]int),
		lg:    dr.lg,
	}
	dr.handlerM[folder.Dir()] = rfr
	return rfr
}

func (dr *dirRecorderT) CloseDir(folder ftw.AbsFolder) error {
	rfr, ok := dr.handlerM[folder.Dir()]
	if !ok {
		return errors.New(fmt.Sprintf("handler for folder %q not found", folder.Dir()))
	}
	// if fr.verbose {
	// 	// log.Printf("fileRecorderT.Close(): %#v", fr)
	dr.lg.Debug("FileRecorderT.Close()", "found", rfr.found)
	// }
	for _, mustFind := range rfr.spec.mustFind {
		if rfr.found[mustFind] <= 0 {
			return errors.New(fmt.Sprintf("Did not find %q", mustFind))
		}
	}
	for _, mustNotFind := range rfr.spec.mustNotFind {
		if rfr.found[mustNotFind] > 0 {
			return errors.New(fmt.Sprintf("Found %q", mustNotFind))
		}
	}
	return nil
}

func (fr *fileRecorderT) HandleFile(relPath string, fmode os.FileMode) error {
	// if fr.verbose {
	// lg.Printf("fileRecorderT.ScanFile(%q, %d)", fpath, fmode)
	fr.lg.Debug("FileRecorderT.ScanFile()",
		// "relpath", fr.dir.Rel(fpath),
		//  "fpath", fpath,
		"relpath", relPath,
		"fmode", fmode)
	// }
	fr.found[filepath.Base(relPath)]++
	return nil
}
