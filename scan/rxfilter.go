package scan

import (
	"regexp"

	"gitlab.com/zx42/go-deps/log"
)

type (
	regexMatcherT struct {
		*regexp.Regexp
	}
)

var (
	RegEx regexMatcherT
)

//
// filterT struct
//
func NewRegExFilter(name string, lg log.Logger, filterRegExL ...string) *filterSpecT {
	return NewGenericFilter(FtFilter, name, RegEx, filterRegExL, lg)
}

func NewRegExSkipper(name string, lg log.Logger, filterRegExL ...string) *filterSpecT {
	return NewGenericFilter(FtSkipper, name, RegEx, filterRegExL, lg)
}

func NewRegExTeeFilter(name string, lg log.Logger, filterRegExL ...string) *filterSpecT {
	return NewGenericFilter(FtTeeFilter, name, RegEx, filterRegExL, lg)
}

func (rxm regexMatcherT) NewMatcher(rxS string) Matcher {
	return regexMatcherT{
		Regexp: regexp.MustCompile(rxS),
	}
}

func (rxm regexMatcherT) QuoteMeta(s string) string {
	qms := regexp.QuoteMeta(s)

	// msg := fmt.Sprintf("regexMatcher.QuoteMeta(%q): %q", s, qms)
	// panic(msg)

	// dotIdx := strings.Index(s, ".")
	// if dotIdx >= 0 {
	// 	msg := fmt.Sprintf("regexMatcher.QuoteMeta(%q): %q", s, qms)
	// 	panic(msg)
	// }
	return qms
}
