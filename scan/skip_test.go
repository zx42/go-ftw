// Copyright © 2019 Hendrik Radon <mrcs.sanefiles@mailnull.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package scan_test

import (
	"context"
	"testing"

	"gitlab.com/zx42/go-ftw"
	. "gitlab.com/zx42/go-mockfs"

	"gitlab.com/zx42/go-ftw/scan"
	"gitlab.com/zx42/go-ftw/walk"
)

// var (
// 	// lg = log.GetLogger()
// 	lg = log.GetDebugLogger()
// )

func TestScanFilesSkip(t *testing.T) {
	tests := []struct {
		name    string
		dir     FE
		frec    ftw.DirHandler
		wantErr bool
		// verbose bool
	}{
		// `/\$Recycle.Bin$`,
		{name: "skip-all", dir: D(`11`, F("c")),
			frec: scan.FileRec(true, lg).MustNot("c")},
		{name: "skip-subdir", dir: D("a", D(`22`, F("c")), F("d")),
			frec: scan.FileRec(false, lg).Must("a", "d").MustNot("c")},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fs := FS("skip", tt.dir)
			fs.Create()

			// skipper := scan.NewSkipper(core.SkipWalkRegExsL)
			skipper := scan.NewRegExSkipper("dirty-filenames", lg, scan.DirtyTestFileNames()...)
			lg.Debug("scan.NewSkipper()", "skipper", skipper)

			errH := scan.IgnoreMoreContextErrors(scan.NewErrorPrinter(lg), lg)

			// mfs := ctrl.NewMultiFolderScanner(errH, aborter, tt.frec)
			// err := mfs.ScanFiles(cxt, fs.Path())
			// frec := tt.frec.WithinDir(fs.Path())
			// skipHandler := skipper.WithinDir(fs.Path())
			ftwr := walk.NewParDirWalker(lg, errH, skipper, tt.frec)
			err := ftwr.WalkPar(context.TODO(), fs.Path())

			// mfs := ctrl.NewMultiFolderScanner(scan.ErrorPrinter, skipper, tt.frec) // .Must("skip"))
			// err := mfs.ScanFiles(context.Background(), fs.Path())
			if (err != nil) != tt.wantErr {
				t.Errorf("ScanFilesSkip(%s) error = %v, wantErr %v", fs.Path(), err, tt.wantErr)
				return
			}

			// skipMatcher, ok := skipper.(ftw.FileMatcher)
			// if ok {
			lg.Debug("TestScanFilesSkip()", "test", tt.name,
				"matched", skipper.MatchedFiles(ftw.NewFolder(fs.Path())))
			// }

			scan.FinishTest("Skip", t, fs, tt.frec)
		})
	}
}
