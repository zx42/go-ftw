package scan

import (
	"context"
	"os"

	"gitlab.com/zx42/go-ftw"
)

type (
	contextCheckerT struct {
		cxt context.Context
	}
)

func NewContextChecker(cxt context.Context) *contextCheckerT {
	ccr := &contextCheckerT{cxt: cxt}
	return ccr
}

func (ccr *contextCheckerT) OpenDir(folder ftw.AbsFolder) ftw.FileHandler {
	return ccr
}

func (ccr *contextCheckerT) HandleFile(_ string, _ os.FileMode) error {
	select {
	case <-ccr.cxt.Done():
		return ccr.cxt.Err()

	default:
		return nil
	}
}

// func (ccr *contextCheckerT) Close() error {
// 	return nil
// }
