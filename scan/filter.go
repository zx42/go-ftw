package scan

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/zx42/go-deps/log"
	"gitlab.com/zx42/go-ftw"
)

type (
	Matcher interface {
		String() string
		MatchString(sample string) bool
	}
	MatcherType interface {
		NewMatcher(expressionS string) Matcher
		QuoteMeta(expressionS string) string
	}

	filterSpecT struct {
		Name               string
		FileNameFilterExSL []string
		MatchType          MatcherType
		MatchErr           error
		NonmatchErr        error

		filterM map[string]*filterT
		lg      log.Logger
	}

	filterT struct {
		spec         *filterSpecT
		fexL         filterMatcherLT
		matchedFiles []string
		lg           log.Logger
	}

	filterExprT struct {
		ExprS     string `json:"expr,omitempty"`
		ReturnErr error  `json:"-"`
	}

	filterMatcherLT []filterMatcherT
	filterMatcherT  struct {
		matcher   Matcher
		returnErr error
	}

	FilterType int
)

const (
	FtUnknown FilterType = iota
	FtFilter
	FtSkipper
	FtTeeFilter
)

//
// filterRxLT
//
func (frxl filterMatcherLT) String() string {
	strL := make([]string, len(frxl))
	for i, rx := range frxl {
		strL[i] = rx.matcher.String()
	}
	s := strings.Join(strL, "', '")
	return fmt.Sprintf("['%s']", s)
}

//
// filterT struct
//
func NewGenericFilter(fltType FilterType, name string, matchType MatcherType, fileNameFilterExL []string, lg log.Logger) *filterSpecT {
	fts := initFilter(name, matchType, fileNameFilterExL, lg)
	switch fltType {
	case FtFilter:
		fts.NonmatchErr = ftw.SkipFile
	case FtSkipper:
		fts.MatchErr = ftw.SkipDir
	case FtTeeFilter:
		fts.MatchErr = ftw.SkipFile
	default:
		panic("Unknown filter type")
	}
	return fts
}

// func new_Skipper(name string, matchType MatcherType, fileNameFilterExL []string) *filterSpecT {
// 	fts := init_Filter(name, matchType, fileNameFilterExL)
// 	fts.MatchErr = ftw.SkipDir
// 	return fts
// }

// func new_TeeFilter(name string, matchType MatcherType, fileNameFilterExL []string) *filterSpecT {
// 	fts := init_Filter(name, matchType, fileNameFilterExL)
// 	fts.MatchErr = ftw.SkipFile
// 	return fts
// }

// func NewGenericFilter(name string, matchType MatcherType, fileNameFilterExL []string, lg log.Logger) *filterSpecT {
// 	return initFilter(name, matchType, fileNameFilterExL, lg)
// }

func initFilter(name string, matchType MatcherType, fileNameFilterExL []string, lg log.Logger) *filterSpecT {
	return &filterSpecT{
		Name:               name,
		FileNameFilterExSL: fileNameFilterExL,
		MatchType:          matchType,
		filterM:            make(map[string]*filterT),
		lg:                 lg,
	}
}

func (fts *filterSpecT) String() string {
	return fmt.Sprintf("Filter[%s]{%q}", fts.Name, fts.RegExSL)
}

func (fts *filterSpecT) OpenDir(folder ftw.AbsFolder) ftw.FileHandler {
	// fel := asFilterExprL(fts.MatchErr, fts.FileNameFilterExSL, folder, fts.MatchType)
	filterExprL := fts.asFilterExprL(folder)
	if len(filterExprL) == 0 {
		return nil
	}
	ftr := fts.newFilterEx(filterExprL)
	fts.filterM[folder.Dir()] = ftr
	return ftr
}

// func asFilterExprL(err error, filterRegExL []string, folder ftw.AbsFolder, matchType MatcherType) []filterExprT {
func (fts *filterSpecT) asFilterExprL(
	// err error,
	// filterRegExL []string,
	folder ftw.AbsFolder,
	// matchType MatcherType,
) []filterExprT {
	fel := make([]filterExprT, 0, len(fts.FileNameFilterExSL))
	for _, reS := range fts.FileNameFilterExSL {
		if reS == "" {
			continue
		}

		if filepath.IsAbs(reS) {
			normExprS, pos := FindExStart(reS, fts.MatchType, fts.lg)
			if pos >= 0 {
				pathStart := normExprS[:pos]
				if strings.HasPrefix(pathStart, folder.Dir()) {
					reS0 := reS
					reS = normExprS[len(folder.Dir())+1:]
					// log.Printf("asFilterExprL(): reS=%q, was %q before", reS, reS0)
					fts.lg.Debug("filterSpecT.asFilterExprL(): trimming expr", "name", fts.Name, "new-expr", reS, "old-expr", reS0)
				} else if !strings.HasPrefix(folder.Dir(), pathStart) {
					// the file tree specified by `reS` is not a super tree of the file tree being walked here
					// but another unrelated subtree
					// so let's ignore this filter expression altogether
					fts.lg.Info("filterSpecT.asFilterExprL(): IGNORING expr",
						"name", fts.Name, "folder", folder.Dir(), "expr", reS)
					continue
				}
			}
		}
		fel = append(fel, filterExprT{ExprS: reS, ReturnErr: fts.MatchErr})
	}
	return fel
}

func (ftSpec *filterSpecT) newFilterEx(
	// spec *filterSpecT,
	filterExprL []filterExprT,
) *filterT {
	// log.Printf("newFilterEx(defaultErr=%v, filterExprL=%+v)",
	// 	defaultErr, filterExprL)
	if len(filterExprL) == 0 {
		return nil
	}

	ftr := &filterT{
		spec: ftSpec,
		fexL: make(filterMatcherLT, 0, len(filterExprL)),
		lg:   ftSpec.lg,
	}
	ftSpec.lg.Debug("filterSpecT.newFilterEx()", "name", ftSpec.Name, "filterExprL", filterExprL)
	for _, fex := range filterExprL {
		// log.Printf("NewFilter(skip=%v, %q) add...", skip, reS)
		fex := filterMatcherT{
			// rx:        regexp.MustCompile(fex.ExprS),
			matcher:   ftSpec.MatchType.NewMatcher(fex.ExprS),
			returnErr: fex.ReturnErr,
		}
		ftr.fexL = append(ftr.fexL, fex)
	}
	return ftr
}

func (ftr *filterT) HandleFile(relPath string, fmode os.FileMode) error {
	// normPath := filepath.ToSlash(ftr.dir.Rel(fpath))
	normPath := filepath.ToSlash(relPath)

	switch {
	case ftr == nil:
		panic("filterT.HandleFile: ftr==nil")
	case ftr.lg == nil:
		panic("filterT.HandleFile: ftr.lg==nil")
	case ftr.spec == nil:
		panic("filterT.HandleFile: ftr.spec==nil")
	}
	ftr.lg.Debug("filterT.ScanFile()", "name", ftr.spec.Name, "relPath", relPath, "normPath", normPath)

	for _, fex := range ftr.fexL {
		// matches := fex.rx.MatchString(normPath)
		matches := fex.matcher.MatchString(normPath)
		// log.Printf("filterT.ScanFile(%q): match %q = %v",
		// 	fpath, fex.rx, matches)
		if matches {
			// log.Printf("filterT.ScanFile(%q): matches %q, return %v",
			// 	fpath, fex.rx, fex.returnErr)
			ftr.lg.Debug("filterT.ScanFile(): match", "name", ftr.spec.Name, "relPath", relPath, "normPath", normPath,
				"expr", fex.matcher.String(), "return", errStr(fex.returnErr))

			ftr.matchedFiles = append(ftr.matchedFiles, relPath)
			// lg.Debug("filterT.ScanFile(): match", "name", ftr.name, "match-files", ftr.files)
			// lg.Debug("filterT.ScanFile(): match", "name", ftr.name, "match-files()", ftr.MatchedFiles())
			return fex.returnErr
		}
	}
	// log.Printf("filterT.ScanFile(%q): no-match, return %v",
	// 	fpath, ftr.defaultErr)
	ftr.lg.Debug("filterT.ScanFile(): no match", "name", ftr.spec.Name, "relPath", relPath, "normPath", normPath,
		"exprL", ftr.fexL, "return", errStr(ftr.spec.NonmatchErr))
	return ftr.spec.NonmatchErr
}

func errStr(err error) string {
	errS := "err:nil"
	if err != nil {
		errS = err.Error()
	}
	return errS
}

func (fts *filterSpecT) MatchedFiles(folder ftw.AbsFolder) []string {
	ftr, ok := fts.filterM[folder.Dir()]
	if ok {
		ftr.lg.Debug("filterT.MatchedFiles()", "name", fts.Name, "folder", folder.Dir(),
			"filter", fmt.Sprintf("%p", ftr), "match-files", ftr.matchedFiles)
		return ftr.matchedFiles
	}
	ftr.lg.Error("filterT.MatchedFiles(): UNKNOWN folder", "name", fts.Name, "folder", folder.Dir())
	return nil
}

// func (ftr *filterT) Close() error {
// 	return nil
// }
