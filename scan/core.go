package scan

import (
	"context"
	"fmt"
	"io"
	"runtime"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/zx42/go-deps/log"
	. "gitlab.com/zx42/go-mockfs"

	"gitlab.com/zx42/go-ftw"
)

var (
	SkipTrashCvsL = []string{
		`\.git$`, `/\.svn$`, `/\.hg$`, `/\.bzr$`, `/CVS$`,
		`/\.DS_Store`, `/\.Trash$`, `/\.Trash-[0-9]+$`, `/Trash$`,
		`(?i)/\$RECYCLE\.BIN$`,
		// `/\$Recycle.Bin$`,
		`/node_modules$`, `/bower_components$`,
	}

	// allDirtyRE         = `[^a-z0-9\.\-]`
	allDirtyRE = `[^a-z\./]`
	// fileNameCleanSpecL = []core.FileNameCleanSpecT{
	// 	core.FileNameCleanSpecT{DirtyRegEx: allDirtyRE},
	fileNameCleanSpecL = [][]string{
		{allDirtyRE},
		{`1`, "a"}, {`2`, "b"}, {`3`, "c"},
		{`4`, "dddd"},
		{`55555`, "e"},
		// {`A`, "a"}, {`B`, "b"}, {`C`, "c"},
		// {`D`, "dddd"},
		// {`EEEEE`, "e"},
		{allDirtyRE, "_"},
	}
	// lg = log.GetLogger()
)

//
// some Helpers
//
func DirtyTestFileNames() []string {
	dfnL := make([]string, 0, 10)
	for _, dfn := range fileNameCleanSpecL {
		dfnL = append(dfnL, dfn[0])
	}
	return dfnL
}

// func FinishTest(name string, t *testing.T, fs FSys, frec ftw.DirHandler) {
func FinishTest(name string, t *testing.T, fs FSys, frec interface{}) {
	var err error
	switch closer := frec.(type) {
	case ftw.DirCloser:
		err = closer.CloseDir(ftw.NewFolder(fs.Path()))
		if err != nil {
			err = errors.Wrap(err, "CloseDir() failed")
		}
	case io.Closer:
		err = closer.Close()
		if err != nil {
			err = errors.Wrap(err, "Close() failed")
		}
	default:
		err = errors.New(fmt.Sprintf("UNKNOWN frec type: %T %#v", frec, frec))
	}
	if err != nil {
		t.Errorf("%q.Close(%q): error = '%v'", name, fs.Path(), err)
		return
	}

	if runtime.GOOS != "windows" {
		fs.Discard()
	}
}

func NewErrorPrinter(lg log.Logger) func(fpath string, err error) error {
	return func(fpath string, err error) error {
		// log.Printf("ERROR: %q[%d]: %q: %#v", fpath, len(fpath), err, err)
		// log.Printf("ERROR: %q[%d]: %#v", fpath, len(fpath), err)
		lg.Error("ErrorPrinter", "fpath", fpath, "len(fpath)", len(fpath), "err", err)
		return nil
		// log.Printf("ERROR: %q: %s", fpath, err)
		// return nil
	}
}

// func ErrorPrinter(fpath string, err error) error {
// 	// log.Printf("ERROR: %q[%d]: %q: %#v", fpath, len(fpath), err, err)
// 	// log.Printf("ERROR: %q[%d]: %#v", fpath, len(fpath), err)
// 	lg.Error("ErrorPrinter", "fpath", fpath, "len(fpath)", len(fpath), "err", err)
// 	return nil
// 	// log.Printf("ERROR: %q: %s", fpath, err)
// 	// return nil
// }

func IgnoreErrors(fpath string, err error) error {
	return nil
}

// func IgnoreAllErrors(errHdlr ftw.ErrorHandler) ftw.ErrorHandler {
// 	return func(fpath string, err error) error {
// 		err = errHdlr(fpath, err)
// 		if err != nil {
// 			err = ErrorPrinter(fpath, err)
// 		}
// 		return err
// 	}
// }

func IgnoreContextErrors(errHdlr ftw.ErrorHandler) ftw.ErrorHandler {
	return func(fpath string, err error) error {
		if IsContextError(err) {
			return err
		}
		return errHdlr(fpath, err)
	}
}

func IsContextError(err error) bool {
	err0 := errors.Cause(err)
	ice := err0 == context.Canceled || err0 == context.DeadlineExceeded
	// log.Printf("IsContextError(%T %#v): %v, cause=%T %#v", err, err, ice, err0, err0)
	return ice
}

func IgnoreMoreContextErrors(errHdlr ftw.ErrorHandler, lg log.Logger) ftw.ErrorHandler {
	return func(fpath string, err error) error {
		if IsRelaxedContextError(err, lg) {
			return err
		}
		return errHdlr(fpath, err)
	}
}

func IsRelaxedContextError(err error, lg log.Logger) bool {
	// ice := IsContextError(err)
	err0 := errors.Cause(err)
	errMsg := err0.Error()
	iceRelaxed := errMsg == context.Canceled.Error() || errMsg == context.DeadlineExceeded.Error()
	result := iceRelaxed || IsContextError(err)
	// log.Printf("IsRelaxedContextError(%T %#v): %v, cause=%T %#v", err, err, ice, err0, err0)
	lg.Info("IsRelaxedContextError", "result", result,
		"err", err, "errType", fmt.Sprintf("%T", err),
		"cause", err0, "causeType", fmt.Sprintf("%T", err0))
	return result
}
