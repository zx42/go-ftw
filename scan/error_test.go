// Copyright © 2019 Hendrik Radon <mrcs.sanefiles@mailnull.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package scan_test

import (
	"context"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/zx42/go-ftw"
	. "gitlab.com/zx42/go-mockfs"

	"gitlab.com/zx42/go-ftw/scan"
	"gitlab.com/zx42/go-ftw/walk"
)

type (
	troubleMakerT struct {
		// dir     ftw.AbsFolder
		errorAt string
		err     error
	}
)

var (
	errFile = errors.New("Failed to access file")
)

func TestScanFilesError(t *testing.T) {
	tests := []struct {
		name   string
		dir    FE
		errAt  string
		frec   ftw.DirHandler
		expErr error
	}{
		{name: "err-1", dir: D("a", D("b", F("c")), F("d")),
			errAt: "a/b",
			frec:  scan.FileRec(false, lg).Must("a", "d").MustNot("b"),
		},
		{name: "err-2", dir: D("a", D("b"), F("c"), F("d")),
			errAt: "a/b",
			frec:  scan.FileRec(false, lg).Must("a", "c", "d").MustNot("b"),
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fs := FS("error", tt.dir).Create()
			tmr := NewErr(tt.errAt, errFile) //--.WithinDir(fs.Path())
			// frec := tt.frec.WithinDir(fs.Path())
			ftwr := walk.NewParDirWalker(lg, scan.NewErrorPrinter(lg), tmr, tt.frec)
			err := ftwr.WalkPar(context.TODO(), fs.Path())
			if errors.Cause(err) != tt.expErr {
				t.Errorf("ScanFilesError(%s) error = '%v', expected '%v'", fs.Path(), err, tt.expErr)
				return
			}

			scan.FinishTest("Error", t, fs, tt.frec)
		})
	}
}

//
// troubleMakererT struct
//
func NewErr(errAt string, err error) *troubleMakerT {
	abr := &troubleMakerT{
		errorAt: errAt,
		err:     err,
	}
	return abr
}

func (tmr *troubleMakerT) OpenDir(folder ftw.AbsFolder) ftw.FileHandler {
	// tmr.dir = folder
	return tmr
}

func (tmr *troubleMakerT) HandleFile(relPath string, _ os.FileMode) error {
	// func (tmr *troubleMakererT) Visit(fEntry core.FEntry) error {
	if tmr.errorAt == "" {
		return nil
	}

	// normPath := filepath.ToSlash(tmr.dir.Rel(fpath))
	normPath := filepath.ToSlash(relPath)
	if strings.HasSuffix(normPath, tmr.errorAt) {
		return tmr.err
	}

	return nil
}

// func (tmr *troubleMakerT) Close() error {
// 	// log.Printf("troubleMakererT.Close(): %#v", tmr)
// 	return nil
// }
