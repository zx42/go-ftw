package scan

import (
	"fmt"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/gobwas/glob"
	"gitlab.com/zx42/go-deps/log"
)

type (
	globMatcherT struct {
		glob.Glob
		globExS string
	}
)

var (
	GlobEx globMatcherT
)

//
// globMatcher struct {
//
func NewGlobFilter(name string, filterGlobExL []string, lg log.Logger) *filterSpecT {
	return NewGenericFilter(FtFilter, name, GlobEx, filterGlobExL, lg)
	// return newFilter(name, GlobEx, filterGlobExL)
}

func NewGlobSkipper(name string, filterGlobExL []string, lg log.Logger) *filterSpecT {
	return NewGenericFilter(FtSkipper, name, GlobEx, filterGlobExL, lg)
	// return new_Skipper(name, GlobEx, filterGlobExL)
}

func NewGlobTeeFilter(name string, filterGlobExL []string, lg log.Logger) *filterSpecT {
	return NewGenericFilter(FtTeeFilter, name, GlobEx, filterGlobExL, lg)
	// return new_TeeFilter(name, GlobEx, filterGlobExL)
}

// func FindGlobEx(globExS string) (string, int) {
func FindExStart(exprS string, matchType MatcherType, lg log.Logger) (string, int) {
	normExprS := filepath.ToSlash(exprS)
	qgexS := matchType.QuoteMeta(normExprS)
	iq := strings.Index(qgexS, "\\")
	if iq < 0 {
		return normExprS, -1
	}

	rexS := "/[^/]*$" // + fps.Glob[iq:]
	rex := regexp.MustCompile(rexS)
	// fmt.Printf("FindStringIndex(%q, %q)\n", rex.String(), fps.Glob[:iq])
	loc := rex.FindStringIndex(normExprS[:iq])
	if len(loc) == 0 {
		lg.Debug("missing scan dir in file path expr",
			"search-str", normExprS[:iq],
			"full-ex", normExprS,
			"meta-rex", rex.String(),
		)
		return "", -1

		// panic("missing scan dir in file path expr: " + normGlobExS)
		msg := fmt.Sprintf("missing scan dir in file path %q expr(%q): %q",
			normExprS, rex.String(), normExprS[:iq])
		panic(msg)
	}
	return normExprS, loc[0]
}

func (glm globMatcherT) NewMatcher(globExS string) Matcher {
	normGlobExS := filepath.ToSlash(globExS)
	return globMatcherT{
		Glob:    glob.MustCompile(normGlobExS, '/'),
		globExS: normGlobExS,
	}
}

func (glm globMatcherT) MatchString(s string) bool {
	m := glm.Match(s)
	// if m {
	// 	log.Printf("globMatcher[%q].MatchString(%q): %v", glm.String(), s, m)
	// }
	return m
}

func (glm globMatcherT) String() string {
	return fmt.Sprintf("GlobMatcher{%q}", glm.globExS)
}

func (glm globMatcherT) QuoteMeta(s string) string {
	qms := glob.QuoteMeta(s)

	// msg := fmt.Sprintf("globMatcher.QuoteMeta(%q): %q", s, qms)
	// panic(msg)

	// dotIdx := strings.Index(s, ".")
	// if dotIdx >= 0 {
	// 	msg := fmt.Sprintf("globMatcher.QuoteMeta(%q): %q", s, qms)
	// 	panic(msg)
	// }
	return qms
}
