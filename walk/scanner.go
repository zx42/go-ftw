// Copyright © 2019 Hendrik Radon <mrcs.sanefiles@mailnull.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package walk

import (
	"context"
	"io"

	"github.com/pkg/errors"
	"gitlab.com/zx42/go-deps/log"
	"golang.org/x/sync/errgroup"

	"gitlab.com/zx42/go-ftw"
)

type (
	multiFolderScannerT struct {
		dhrL    []ftw.DirHandler
		errHdlr ftw.ErrorHandler
		lg      log.Logger
	}
)

func NewParDirWalker(lg log.Logger, errHdlr ftw.ErrorHandler, dhrL ...ftw.DirHandler) *multiFolderScannerT {
	return &multiFolderScannerT{
		dhrL: dhrL, errHdlr: errHdlr, lg: lg,
	}
}

func (mfs *multiFolderScannerT) WalkPar(cxt context.Context, folders ...string) error {
	mfs.lg.Debug("multiFolderScannerT.WalkPar()", "folders", folders)
	errGroup, cxt := errgroup.WithContext(cxt)
	seqFtw := NewSeqDirWalker(mfs.lg, mfs.errHdlr, mfs.dhrL...)
	for _, folder := range folders {
		absFolder := ftw.NewFolder(folder)
		fileHandlersL := seqFtw.withinDir(cxt, absFolder)

		errGroup.Go(func() error {
			err := seqFtw.doWalkSeq(
				fileHandlersL,
				absFolder.Dir(), nil,
			)
			mfs.lg.Debug("multiFolderScannerT.WalkPar(): walk done", "folder", absFolder.Dir())

			if err != nil {
				return errors.Wrap(err, "walking file tree")
			}

			// for _, hdlr := range mfs.dhrL {
			// 	dirCloser, ok := hdlr.(ftw.DirCloser)
			// 	if ok {
			// 		err = dirCloser.CloseDir(absFolder)
			// 	}
			// 	if err != nil {
			// 		return errors.Wrap(err, "Closing file visitor")
			// 	}
			// }
			return nil
		})
	}

	err := errGroup.Wait()
	if err != nil {
		return errors.Wrap(err, "Walking file tree concurrently")
	}

	for _, folder := range folders {
		absFolder := ftw.NewFolder(folder)
		mfs.lg.Debug("multiFolderScannerT.WalkPar(): closing dir", "folder", absFolder.Dir())
		for i := range mfs.dhrL {
			hdlr := mfs.dhrL[len(mfs.dhrL)-1-i]
			dirCloser, ok := hdlr.(ftw.DirCloser)
			if !ok {
				continue
			}

			// if ok {
			err = dirCloser.CloseDir(absFolder)
			// }
			if err != nil {
				return errors.Wrap(err, "Closing file visitor")
			}
		}
	}
	return nil
}

func (mfs *multiFolderScannerT) Close() error {
	for i := range mfs.dhrL {
		hdlr := mfs.dhrL[len(mfs.dhrL)-1-i]
		waiter, ok := hdlr.(ftw.Waiter)
		if ok {
			waiter.Wait()
		}
		closer, ok := hdlr.(io.Closer)
		if !ok {
			continue
		}
		// if ok {
		err := closer.Close()
		if err != nil {
			return errors.Wrap(err, "Closing dir visitor")
		}
		// }
	}
	return nil
}
