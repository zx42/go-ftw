package walk

import (
	"context"
	"fmt"

	"github.com/karrick/godirwalk"
	"github.com/pkg/errors"

	"gitlab.com/zx42/go-deps/log"
	"gitlab.com/zx42/go-ftw"
	"gitlab.com/zx42/go-ftw/scan"
)

type (
	// * Architecture: Stackable DirHandler:
	//   * several file tree walk algorithms
	// FileTreeWalker interface {
	// 	// HandleDir(cxt context.Context, dirpath string) error
	// 	WalkDir(cxt context.Context, dirpath string) error
	// }
	// FileHandleFunc func(fpath string, fmode os.FileMode) error

	// stdFileTreeWalkT struct {
	// 	// fsrL []core.FileScanner
	// 	fsrL    []core.FileVisitor
	// 	errHdlr core.ErrorHandler
	// }

	goDirWalkT struct {
		dhrL        []ftw.DirHandler
		errCallback func(string, error) godirwalk.ErrorAction
		lg          log.Logger
	}
)

//
// goDirWalkFacT struct
//
func NewSeqDirWalker(lg log.Logger, errHdlr ftw.ErrorHandler, dhrs ...ftw.DirHandler) *goDirWalkT {
	gdw := &goDirWalkT{
		dhrL: dhrs,
		lg:   lg,
	}
	if errHdlr != nil {
		gdw.errCallback = func(fpath string, err error) godirwalk.ErrorAction {
			err = errHdlr(fpath, err)
			if err == nil || isSkip(err) {
				return godirwalk.SkipNode
			}
			return godirwalk.Halt
		}
	}
	return gdw
}

func isSkip(err error) bool {
	return errors.Cause(err) == ftw.SkipDir || errors.Cause(err) == ftw.SkipFile
}

//
// goDirWalkT struct
//
func (gdw *goDirWalkT) WalkSeq(
	baseDir string, dirs ...string,
) error {
	absBaseFolder := ftw.NewFolder(baseDir)
	return gdw.doWalkSeq(
		gdw.withinDir(nil, absBaseFolder),
		baseDir, dirs,
	)
}

func (gdw *goDirWalkT) doWalkSeq(
	fileHandlerL []ftw.FileHandler,
	baseDir string, dirs []string,
) error {
	absBaseFolder := ftw.NewFolder(baseDir)

	var walkDirsL []string
	if len(dirs) == 0 {
		walkDirsL = []string{absBaseFolder.Dir()}
	} else {
		for _, dir := range dirs {
			walkDirsL = append(walkDirsL, absBaseFolder.Join(dir))
		}
	}

	for _, walkDirS := range walkDirsL {
		err := godirwalk.Walk(walkDirS,
			&godirwalk.Options{
				Callback: func(osPathname string, directoryEntry *godirwalk.Dirent) error {
					relPath := absBaseFolder.Rel(osPathname)
					for i, fileHandler := range fileHandlerL {
						if isNilFileHandler(fileHandler) {
							gdw.lg.Debug("goDirWalkT.Walk(): fileHandler == nil", "no", i, "walkDirS", walkDirS)
							continue
						}

						gdw.lg.Debug("godirwalk.doWalkSeq(): HandleFile",
							"relPath", relPath, "fmode", directoryEntry.ModeType(),
							"scanner", fmt.Sprintf("%T: @ %p", fileHandler, fileHandler),
						)
						err := fileHandler.HandleFile(relPath, directoryEntry.ModeType())
						if err != nil {
							if err == ftw.SkipFile {
								break
							}
							return err
						}
					}
					return nil
				},
				ErrorCallback: gdw.errCallback,
			},
		)
		if err != nil {
			return errors.Wrapf(err, "Walking dir %q", walkDirS)
		}
	}
	return nil
}

func (gdw *goDirWalkT) withinDir(cxt context.Context, absFolder ftw.AbsFolder) (fileHandlerL []ftw.FileHandler) {
	if cxt != nil {
		fileHandlerL = append(fileHandlerL, scan.NewContextChecker(cxt))
	}
	for i, dirHandler := range gdw.dhrL {
		if isNilDirHandler(dirHandler) {
			gdw.lg.Debug("goDirWalkT.withinDir(): dirHandler == nil", "no", i)
			continue
		}

		fileHandler := dirHandler.OpenDir(absFolder)
		if isNilFileHandler(fileHandler) {
			gdw.lg.Debug("goDirWalkT.withinDir(): fileHandler == nil", "no", i, "absFolder", absFolder.Dir())
			continue
		}

		fileHandlerL = append(fileHandlerL, fileHandler)
	}
	return fileHandlerL
}

func isNilFileHandler(fh ftw.FileHandler) bool {
	return fh == nil
}

func isNilDirHandler(dh ftw.DirHandler) bool {
	return dh == nil
}
