package ftw

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

type (
	// FileTreeWalkerFactory interface {
	// 	NewFileTreeWalker(errHdlr ErrorHandler, fsrs ...FileScanner) FileTreeWalker
	// }

	// FileTreeWalker interface {
	// 	Walk(dirpath string) error
	// }
	ErrorHandler func(string, error) error

	// // FileScanner interface {
	// // 	io.Closer
	// // 	// SetBaseDir(baseDir string)
	// // 	WithinDir(baseDir string) FileScanner
	// // 	ScanFile(fpath string, fmode os.FileMode) error
	// // }

	// FileHandler interface {
	// 	io.Closer
	// 	// SetBaseDir(baseDir string)
	// 	// WithinDir(baseDir string) FileScanner
	// 	HandleFile(fpath string, fmode os.FileMode) error
	// }

	DirHandler interface {
		OpenDir(folder AbsFolder) FileHandler
	}
	FileHandler interface {
		HandleFile(relPath string, fmode os.FileMode) error
	}
	DirCloser interface {
		CloseDir(folder AbsFolder) error
	}
	Waiter interface {
		Wait()
	}
	// FileHandleFunc func(fpath string, fmode os.FileMode) error

	// AbsFileHandler interface {
	// 	FileHandler
	// 	// SetBaseDir(baseDir string)
	// 	WithinDir(folderS string) FileHandler
	// 	// ScanFile(fpath string, fmode os.FileMode) error
	// 	CloseDir(folderS string) error
	// }

	// FileHandlerFactory interface {
	// 	GetFileHandler() FileHandler
	// }

	FileMatcher interface {
		MatchedFiles(folder AbsFolder) []string
	}

	AbsFolder interface {
		Abs(relPath string) string
		Rel(fpath string) string
		Dir() string
		Join(relPaths ...string) string
	}

	absFolderT struct {
		absNativeBaseDir string
	}
)

var (
	SkipDir  = filepath.SkipDir
	SkipFile = errors.New("Skip File")
)

//
// FolderT struct
//
func NewFolder(folderS string) *absFolderT {
	fdr := &absFolderT{
		absNativeBaseDir: MustStr(filepath.Abs(folderS)),
	}
	fdr.check()
	return fdr
}

func (fdr *absFolderT) String() string {
	return fdr.Dir()
}

func (fdr *absFolderT) Dir() string {
	fdr.check()
	absDir := MustStr(filepath.Abs(fdr.absNativeBaseDir))
	return filepath.ToSlash(absDir)
}

func (fdr *absFolderT) Rel(fpath string) string {
	// TODO: check if path is already relative to base and return?
	fdr.check()
	relDir := MustStr(filepath.Rel(fdr.absNativeBaseDir, fpath))
	return filepath.ToSlash(relDir)
}

func (fdr *absFolderT) Abs(relPath string) string {
	return fdr.Join(relPath)
}

func (fdr *absFolderT) Join(relPaths ...string) string {
	fdr.check()
	paths := append([]string{fdr.absNativeBaseDir}, relPaths...)
	jPaths := make([]string, 0, len(paths))
	for _, p := range paths {
		if filepath.IsAbs(p) {
			jPaths = []string{p}
		} else {
			jPaths = append(jPaths, p)
		}
	}
	fullPath := filepath.Join(jPaths...)
	return filepath.ToSlash(fullPath)
}

func (fdr *absFolderT) check() {
	if fdr.absNativeBaseDir == "" {
		panic("need to set AbsNativeBaseDir (using WithinDir())")
	}
}

func FStr(fpath, baseDir string, fmode os.FileMode) string {
	modeS := "f"
	if fmode.IsDir() {
		modeS = "d"
	}
	return fmt.Sprintf("%s:%s", modeS, RelPath(fpath, baseDir))
}

func RelPath(fpath, baseDir string) string {
	if baseDir == "" {
		return fpath
	}
	return MustStr(filepath.Rel(baseDir, fpath))
}

func MustStr(s string, err error) string {
	if err == nil {
		return s
	}
	panic(err)
}
